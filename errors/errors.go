package errors

import (
	"errors"
	"fmt"
)

type InvalidBidPrice struct {
	MinPrice  float64
	CurrPrice float64
}

func (e InvalidBidPrice) Error() string {
	return fmt.Sprintf("Cannot bid for lower price %f for the item. Minimum Bid Price: %f", e.CurrPrice, e.MinPrice)
}

type InvalidItem struct {
	ItemID string
	Cause  error
}

func (e InvalidItem) Error() string {
	return fmt.Sprintf("Can't create bid for item: %s cause: %s", e.ItemID, e.Cause.Error())
}

type InvalidUser struct {
	UserID string
}

func (e InvalidUser) Error() string {
	return fmt.Sprintf("Invalid User Id: %s", e.UserID)
}

var ItemNotFound = errors.New("Item Not Found")
var AuctionExists = errors.New("Auction already exists")
var BidNotFound = errors.New("No Bid Found for the item")
