package ping

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPing(t *testing.T) {
	r, _ := http.NewRequest(http.MethodGet, "/ping", nil)
	w := httptest.NewRecorder()

	PingHandler(w, r)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, `{"response":"pong"}`, w.Body.String())
}
