package logger

import (
	"io"
	"log"

	"bitbucket.org/devdinu/auction_service/config"
)

type Logger interface {
	Printf(string, ...interface{})
}

var levels = map[string]int{"off": 0, "error": 1, "info": 2, "debug": 9, "all": 10}

var logger Logger

func Setup(w io.Writer, prefix string) {
	logger = log.New(w, prefix, log.LstdFlags)
}

func Infof(format string, args ...interface{}) {
	if levels[config.LogLevel()] >= levels["info"] {
		logger.Printf("[Info] "+format+"\n", args...)
	}
}

func Errorf(format string, args ...interface{}) {
	if levels[config.LogLevel()] >= levels["error"] {
		logger.Printf("[Error] "+format+"\n", args...)
	}
}

func Debugf(format string, args ...interface{}) {
	if levels[config.LogLevel()] >= levels["debug"] {
		logger.Printf("[Debug] "+format+"\n", args...)
	}
}
