package config

import (
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"github.com/kelseyhightower/envconfig"
)

var app application

type application struct {
	service
	logger
}

type logger struct {
	Level  string `default:"error"`
	File   string `default:""`
	Prefix string `default:"service"`
}

type service struct {
	Port int `default:"8080"`
}

func Load() error {
	if err := envconfig.Process("SERVICE", &app.service); err != nil {
		return err
	}
	if err := envconfig.Process("LOG", &app.logger); err != nil {
		return err
	}
	return nil
}

func AppPort() string {
	return fmt.Sprintf(":%d", app.service.Port)
}

func LogLevel() string {
	return strings.ToLower(app.logger.Level)
}

func OutputWriter() io.WriteCloser {
	filename := app.logger.File
	if filename == "" {
		return os.Stdout
	}
	fname := filename + time.Now().Format("2006-01-02")
	f, err := os.OpenFile(fname, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		return os.Stdout
	}
	return f
}
