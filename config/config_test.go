package config

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadConfig(t *testing.T) {
	envVars := map[string]string{
		"SERVICE_PORT": "8888",
	}
	for k, v := range envVars {
		assert.NoError(t, os.Setenv(k, v))
	}

	Load()

	assert.Equal(t, ":8888", AppPort())
}
