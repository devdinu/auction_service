.PHONY: build-all

LOADENV=$(shell cat envfile | xargs)
build-all: install-deps build imports test

DEPINSTALLED := $(shell command -v dep 2> /dev/null)

dep_install:
ifndef DEPINSTALLED
    $(shell go get -u -v github.com/golang/dep/cmd/dep)
endif

install-deps: dep_install
	dep version
	dep ensure -v

build: install-deps
	cd cmd/service/ && go build ./... && cd ../../

lint:
	golint $(go list ./...) | { grep -Ev 'exported.*should have comment.*' || true; }

vet:
	go vet ./...

install:
	cd ./cmd/service/ && go install && cd ../../

imports:
	goimports -l .  | { grep -v vendor || true; }

test:
	go test -v ./...

run: build
	source envfile && ./cmd/service/service

buildDocker:
	docker build -t auction_service_img .

runDocker: buildDocker
	docker run -d --rm -p 8080:8080 --name auction_service auction_service_img:latest
