package bids

import (
	"context"
	"time"

	"bitbucket.org/devdinu/auction_service/errors"
	"bitbucket.org/devdinu/auction_service/logger"
	"bitbucket.org/devdinu/auction_service/store"
	"github.com/satori/go.uuid"
)

type bidStore interface {
	AddBid(context.Context, uuid.UUID, store.Bid) error
	GetItem(context.Context, uuid.UUID) (store.Auction, error)
	ListUserBids(context.Context, uuid.UUID) ([]store.Bid, error)
	ListItemBids(context.Context, uuid.UUID) ([]store.Bid, error)
}

type Service struct {
	bidStore
}

func (s Service) AddBid(ctx context.Context, itemID uuid.UUID, bi bidItem) error {
	item, err := s.bidStore.GetItem(ctx, itemID)
	if err != nil {
		return errors.InvalidItem{ItemID: itemID.String(), Cause: err}
	}
	logger.Debugf("[BidService] adding bid: %v for item: %s price: %f", bi, item.ID, item.Price)
	if item.Price >= bi.Price {
		return errors.InvalidBidPrice{MinPrice: item.Price, CurrPrice: bi.Price}
	}
	userID, err := uuid.FromString(bi.UserID)
	if err != nil {
		return errors.InvalidUser{UserID: bi.UserID}
	}
	newBid := store.Bid{UserID: userID, Price: bi.Price, CreatedAt: time.Now()}
	return s.bidStore.AddBid(ctx, itemID, newBid)
}

func (s Service) ListUserBids(ctx context.Context, userID uuid.UUID) ([]store.Bid, error) {
	return s.bidStore.ListUserBids(ctx, userID)
}

func (s Service) ListItemBids(ctx context.Context, itemID uuid.UUID) ([]store.Bid, error) {
	return s.bidStore.ListItemBids(ctx, itemID)
}

func NewService(store bidStore) Service { return Service{bidStore: store} }
