package bids

import (
	"context"
	"testing"

	"bitbucket.org/devdinu/auction_service/errors"
	"bitbucket.org/devdinu/auction_service/store"
	"github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestAddBidWithLesserPrice(t *testing.T) {
	ctx := context.Background()
	memStore := store.NewMemoryStore()
	svc := Service{memStore}
	itemID, userID := uuid.NewV4(), uuid.NewV4()
	item := store.Auction{Title: "item1", ID: itemID}
	earlierPrice := 100.00

	err := memStore.SaveAuction(ctx, item)
	require.NoError(t, err)

	err = svc.AddBid(ctx, itemID, bidItem{UserID: userID.String(), Price: earlierPrice})
	require.NoError(t, err)

	err = svc.AddBid(ctx, itemID, bidItem{UserID: userID.String()})
	assert.Equal(t, errors.InvalidBidPrice{MinPrice: earlierPrice}, err)
}
