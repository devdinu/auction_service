package bids

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/devdinu/auction_service/logger"
	"github.com/gorilla/mux"
	"github.com/satori/go.uuid"
)

func UserLister(svc Service) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		userID, err := uuid.FromString(mux.Vars(r)["user_id"])
		if userID.String() == "" {
			logger.Errorf("[UserBidLister] Invalid request for listing bids, user id: %s", userID)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		userBids, err := svc.ListUserBids(r.Context(), userID)
		if err != nil {
			logger.Errorf("[UserBidLister] Can't list bids, err: ", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if len(userBids) == 0 {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		data, err := json.Marshal(userBids)
		if err != nil {
			logger.Errorf("[UserBidLister] Can't write response for user: %s", userID)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Write(data)
	})
}

func ItemLister(svc Service) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		itemID, err := uuid.FromString(mux.Vars(r)["item_id"])
		if itemID.String() == "" {
			logger.Errorf("[ItemBidLister] Invalid request for listing bids, item id: %s", itemID)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		itemBids, err := svc.ListItemBids(r.Context(), itemID)
		if err != nil {
			logger.Errorf("[ItemBidLister] Can't list bids, err: ", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if len(itemBids) == 0 {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		data, err := json.Marshal(itemBids)
		if err != nil {
			logger.Errorf("[ItemBidLister] Can't write response for user: %s", itemID)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Write(data)
	})
}
