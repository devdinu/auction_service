package bids

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/devdinu/auction_service/logger"
	"github.com/gorilla/mux"
	"github.com/satori/go.uuid"
)

type bidItem struct {
	UserID string  `json:"user_id"`
	Price  float64 `json:"price"`
}

func BidCreator(svc Service) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var bi bidItem
		id := string(mux.Vars(r)["item_id"])
		itemID, err := uuid.FromString(id)
		if err != nil {
			logger.Errorf("[BidCreator] Bad request for item id: %s, err: %s", id, err.Error())
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		err = json.NewDecoder(r.Body).Decode(&bi)
		if err != nil {
			logger.Errorf("[BidCreator] failed to decode request: %s", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		defer r.Body.Close()

		err = svc.AddBid(r.Context(), itemID, bi)
		if err != nil {
			logger.Errorf("[BidCreator] failed to create bid for item: %s,  cause: %s", itemID, err.Error())
			w.WriteHeader(http.StatusUnprocessableEntity)
			w.Write([]byte(fmt.Sprintf(`{"error":"%s"}`, err.Error())))
			return
		}
		w.WriteHeader(http.StatusCreated)
	})
}

func NewCreator(svc Service) http.HandlerFunc {
	return BidCreator(svc)
}
