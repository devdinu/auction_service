package store

import (
	"context"
	"sync"

	"github.com/satori/go.uuid"
)

type BidStore interface {
	ListUserBids(ctx context.Context, userID uuid.UUID) ([]Bid, error)
	ListItemBids(ctx context.Context, itemID uuid.UUID) ([]Bid, error)
	AddBid(context.Context, uuid.UUID, Bid) error
}

type bidStore struct {
	userBids map[uuid.UUID][]Bid
	itemBids map[uuid.UUID][]Bid
	lock     *sync.RWMutex
}

func (bs bidStore) AddBid(ctx context.Context, itemID uuid.UUID, b Bid) error {
	bs.lock.Lock()
	defer bs.lock.Unlock()

	bs.userBids[b.UserID] = append(bs.userBids[b.UserID], b)
	bs.itemBids[itemID] = append(bs.itemBids[itemID], b)
	return nil
}

func (bs bidStore) Delete(ctx context.Context, bidID uuid.UUID) error {
	return nil
}

func (bs bidStore) ListUserBids(ctx context.Context, userID uuid.UUID) ([]Bid, error) {
	bs.lock.RLock()
	defer bs.lock.RUnlock()

	return bs.userBids[userID], nil
}

func (bs bidStore) ListItemBids(ctx context.Context, itemID uuid.UUID) ([]Bid, error) {
	bs.lock.RLock()
	defer bs.lock.RUnlock()

	return bs.itemBids[itemID], nil
}
func newBidStore() BidStore {
	return bidStore{
		userBids: make(map[uuid.UUID][]Bid),
		itemBids: make(map[uuid.UUID][]Bid),
		lock:     &sync.RWMutex{},
	}
}
