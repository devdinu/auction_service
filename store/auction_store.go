package store

import (
	"context"
	"sync"
	"time"

	"bitbucket.org/devdinu/auction_service/errors"
	"github.com/satori/go.uuid"
)

type memStore struct {
	auctions map[uuid.UUID]*Auction
	BidStore
	al *sync.RWMutex
}

type Auction struct {
	ID          uuid.UUID `json:"id"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	Price       float64   `json:"price"`
	LatestBid   Bid       `json:"last_bid"`
}
type Bid struct {
	UserID    uuid.UUID `json:"user_id"`
	Price     float64   `json:"price"`
	CreatedAt time.Time `json:"created_at"`
}

func (s memStore) SaveAuction(ctx context.Context, auction Auction) error {
	s.al.Lock()
	defer s.al.Unlock()

	existing := s.auctions[auction.ID]
	if existing != nil {
		return errors.AuctionExists
	}
	s.auctions[auction.ID] = &auction
	return nil
}

func (s memStore) AddBid(ctx context.Context, itemID uuid.UUID, bid Bid) error {
	//logger.debugf("[bidstore] adding bid %+v for item %s", bid, itemid)
	s.al.Lock()
	defer s.al.Unlock()

	item := s.auctions[itemID]
	if item == nil {
		return errors.ItemNotFound
	}
	item.Price = bid.Price
	err := s.BidStore.AddBid(ctx, itemID, bid)
	if err != nil {
		return err
	}
	item.LatestBid = bid
	return nil
}

func (s memStore) Winner(ctx context.Context, itemID uuid.UUID) (Bid, error) {
	s.al.RLock()
	defer s.al.RUnlock()
	var bid Bid

	item := s.auctions[itemID]
	if item == nil {
		return bid, errors.ItemNotFound
	}
	if item.LatestBid == bid {
		return bid, errors.BidNotFound
	}
	return item.LatestBid, nil
}

func (s memStore) ListAuctions(ctx context.Context) ([]Auction, error) {
	s.al.RLock()
	defer s.al.RUnlock()

	var auctions []Auction
	for _, v := range s.auctions {
		auctions = append(auctions, *v)
	}
	return auctions, nil
}

func (s memStore) GetItem(ctx context.Context, itemID uuid.UUID) (Auction, error) {
	s.al.RLock()
	defer s.al.RUnlock()

	item := s.auctions[itemID]
	if item == nil {
		return Auction{}, errors.ItemNotFound
	}
	return *item, nil
}

type AuctionStore interface {
	SaveAuction(context.Context, Auction) error
	GetItem(context.Context, uuid.UUID) (Auction, error)
	ListAuctions(context.Context) ([]Auction, error)
	Winner(context.Context, uuid.UUID) (Bid, error)
}

type MemStore interface {
	AuctionStore
	BidStore
}

func NewMemoryStore() MemStore {
	return memStore{
		auctions: make(map[uuid.UUID]*Auction),
		BidStore: newBidStore(),
		al:       &sync.RWMutex{},
	}
}
