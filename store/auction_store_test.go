package store

import (
	"context"
	"os"
	"testing"

	"bitbucket.org/devdinu/auction_service/config"
	"bitbucket.org/devdinu/auction_service/errors"
	"bitbucket.org/devdinu/auction_service/logger"
	"github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func init() {
	config.Load()
	logger.Setup(os.Stdout, "")
}

func TestShouldStoreAuctions(t *testing.T) {
	memstore := NewMemoryStore()
	ctx := context.Background()
	acn1 := Auction{ID: uuid.NewV4(), Title: "title1", Description: "description1"}
	acn2 := Auction{ID: uuid.NewV4(), Title: "title2", Description: "description2"}
	acn3 := Auction{ID: uuid.NewV4(), Title: "title3", Description: "description3"}
	auctions := []Auction{acn1, acn2, acn3}

	for _, a := range auctions {
		err := memstore.SaveAuction(ctx, a)
		require.NoError(t, err)
	}

	savedAuctions, _ := memstore.ListAuctions(ctx)
	require.Equal(t, 3, len(savedAuctions))
	assert.ElementsMatch(t, auctions, savedAuctions)
}

func TestShouldUpdateAuctionLatestBid(t *testing.T) {
	memstore := NewMemoryStore()
	ctx := context.Background()

	itemID, userID := uuid.NewV4(), uuid.NewV4()
	item := Auction{ID: itemID, Title: "title1", Description: "description1"}
	bidPrice := 200.00

	err := memstore.SaveAuction(ctx, item)
	require.NoError(t, err)

	memstore.AddBid(ctx, itemID, Bid{UserID: userID, Price: bidPrice})

	savedAuction, _ := memstore.GetItem(ctx, itemID)
	assert.Equal(t, bidPrice, savedAuction.Price)
}

func TestShouldUpdateUserAndItemBids(t *testing.T) {
	memstore := NewMemoryStore()
	ctx := context.Background()

	itemID, user1, user2 := uuid.NewV4(), uuid.NewV4(), uuid.NewV4()
	item := Auction{ID: itemID, Title: "title1", Description: "description1"}
	bidPrice, lastPrice := 200.00, 1000.00

	err := memstore.SaveAuction(ctx, item)
	require.NoError(t, err)
	bid1user1, user2bid := Bid{UserID: user1, Price: bidPrice}, Bid{UserID: user2, Price: bidPrice + 100}
	lastBidUser1 := Bid{UserID: user1, Price: lastPrice}

	memstore.AddBid(ctx, itemID, bid1user1)
	memstore.AddBid(ctx, itemID, user2bid)
	memstore.AddBid(ctx, itemID, lastBidUser1)

	savedAuction, _ := memstore.GetItem(ctx, itemID)
	assert.Equal(t, lastPrice, savedAuction.Price)

	user1Bids, _ := memstore.ListUserBids(ctx, user1)
	require.Equal(t, 2, len(user1Bids))

	user2Bids, _ := memstore.ListUserBids(ctx, user2)
	require.Equal(t, 1, len(user2Bids))
	assert.Equal(t, Bid{UserID: user2, Price: bidPrice + 100}, user2Bids[0])

	itemBids, _ := memstore.ListItemBids(ctx, itemID)
	require.Equal(t, 3, len(itemBids))
	assert.ElementsMatch(t, []Bid{bid1user1, lastBidUser1, user2bid}, itemBids)
}

func TestWinningBidForItem(t *testing.T) {
	memstore := NewMemoryStore()
	ctx := context.Background()
	itemID, user1, user2 := uuid.NewV4(), uuid.NewV4(), uuid.NewV4()
	highestBid := Bid{UserID: user1, Price: 1000.00}
	bids := []Bid{
		Bid{UserID: user2, Price: 100.00},
		Bid{UserID: user1, Price: 200.00},
		Bid{UserID: user2, Price: 300.00},
		highestBid,
	}
	artwork := Auction{ID: itemID, Title: "artwork", Description: "picasso's artwork"}
	err := memstore.SaveAuction(ctx, artwork)
	require.NoError(t, err)

	for _, b := range bids {
		require.NoError(t, memstore.AddBid(ctx, itemID, b))
	}

	winnerBid, err := memstore.Winner(ctx, itemID)
	require.NoError(t, err)

	assert.Equal(t, highestBid, winnerBid)
}

func TestWinnerWithoutAnyBid(t *testing.T) {
	memstore := NewMemoryStore()
	ctx := context.Background()
	itemID := uuid.NewV4()
	artwork := Auction{ID: itemID, Title: "artwork", Description: "picasso's artwork"}

	err := memstore.SaveAuction(ctx, artwork)
	require.NoError(t, err)

	_, err = memstore.Winner(ctx, itemID)
	require.Equal(t, errors.BidNotFound, err)
}
