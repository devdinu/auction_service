package store_test

import (
	"context"
	"sync"
	"testing"
	"time"

	"bitbucket.org/devdinu/auction_service/store"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func BenchmarkItemBids(b *testing.B) {
	memStore := store.NewMemoryStore()
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	itemID := uuid.NewV4()
	require.NoError(b, memStore.SaveAuction(ctx, store.Auction{ID: itemID}))

	for i := 0; i < b.N; i++ {
		bid := store.Bid{Price: float64(i), UserID: uuid.NewV4()}
		require.NoError(b, memStore.AddBid(ctx, itemID, bid))
	}
}

func TestConcurrency(t *testing.T) {
	memStore := store.NewMemoryStore()
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	itemID := uuid.NewV4()
	require.NoError(t, memStore.SaveAuction(ctx, store.Auction{ID: itemID}))

	concurrentUsers := 5
	var wg sync.WaitGroup
	wg.Add(concurrentUsers)
	for i := 0; i < concurrentUsers; i++ {
		go func(wg *sync.WaitGroup, price float64) {
			defer wg.Done()
			bid := store.Bid{Price: price, UserID: uuid.NewV4()}
			require.NoError(t, memStore.AddBid(ctx, itemID, bid))
		}(&wg, float64(i))
	}
	wg.Wait()

	itemBids, err := memStore.ListItemBids(ctx, itemID)
	require.NoError(t, err)
	assert.Equal(t, concurrentUsers, len(itemBids))
}
