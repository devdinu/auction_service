# Auction Service
Service to auction items

## Requirements
* go  (app developed with version go1.10.3 darwin/amd64)
* dep (installed via makefile)

## Usage
* To install and run tests run `make` which downloads dependencies, and run lint and tests
* To run the service `make run`
* change `envfile` to customize port, and loglevel, and log file
* `./make_requests.sh` sends some sample requests to create auction, and create bids, and lists bids for item and user

### Running application with docker
```
make runDocker
```
* runs the application in `localhost:8080`, `8080` is default port in configuration

#### verifcation
* verify service is up and running by hitting `curl http://localhost:8080/ping` 
should return response
```json
{"response":"pong"}
```
if runnig locally, envfile `SERVICE_PORT` is considered `8000`

## Behaviour

### Assumptions
* User can create multiple bids for same item
* if bid `<=` earlier bid for the item, its ignored, but stored in users history
* item identifier, user identifier are UUID
* before creating bid for an item, item have to be created (auction)
* user resources are not managed, assuming users exists, and user mapping are done with UUID
* user's are not allowed to delete their bid
* listing is not paginated for now

### Storage
* In Memory storage is used, mappings for bids from user and item is stored in bidStore.
* AuctionStore maintains auctions for items and also last bid (which'll be the highest Bid)
* Locks are used to prevent issue with concurrency

### DataStructure
* Simple slice/list datastructure is used, as latest bid is maintained in auction it's sufficient for this usecase.
* The list also gives use simplicity while returing data for particular user/item.
* Hash - `map[uuid.UUID]Auction` is used for holding auctions and `map[uuid.UUID][]Bid` for bids. This consumes more memory, but gives the benefit of getting the list for user/item faster in O(1). This will be mapped to relational Tables, so this mimicks similar behaviour, or easy to extend

### Endpoints

#### Create item for auction 
```
curl -X POST http://localhost:8080/v1/item -d '{"title":"mobile", "description":"mobile version x.x","id":"31d4d6e3-4af7-4ea4-80f6-e4ac139902d8"}' 
```
Sample output, which will have the item ID:
```json
StatusCode: 201
{"id":"ecc9cab0-88a2-48ac-a122-e7e401048591"}
```
id is optional, if none given a new UUID will be generated, this is helpful for testing purpose.

#### Create Bid for an item
Price have to be greater than the last bid for the item, 
if not throws an error `{"error":"Cannot bid for lower price 150.000000 for the item. Minimum Bid Price: 250.000000"}`

```console
curl -X PUT http://localhost:8000/v1/items/ecc9cab0-88a2-48ac-a122-e7e401048591/bid -d '{"user_id":"3DE7D031-B067-4349-95DF-892386C698D2","price":450.00}'
```
Sample output
```
StatusCode: 201
```

#### List items in auction
```
curl http://localhost:8080/v1/items
```
Sample output
```json
[
  {
    "id": "ecc9cab0-88a2-48ac-a122-e7e401048591",
    "title": "art",
    "description": "picasso art 1",
    "price": 450000,
    "last_bid": {
      "user_id": "3de7d031-b067-4349-95df-892386c698d2",
      "price": 450,
      "created_at": "2018-10-20T20:29:14.120480468+05:30"
    }
  }
]
```
This api is exposed for testing, and better use

#### Winner for auction of Item
```
curl http://localhost:8000/v1/items/ecc9cab0-88a2-48ac-a122-e7e401048591/bids/winner
```
Sample Output
```json
{
  "user_id": "3de7d031-b067-4349-95df-892386c698d2",
  "price": 450000,
  "created_at": "2018-10-20T20:29:14.120480468+05:30"
}
```

#### List bids for an item 
```
curl http://localhost:8000/v1/items/ecc9cab0-88a2-48ac-a122-e7e401048591/bids
```
sample output
```json
[
  {
    "user_id": "3de7d031-b067-4349-95df-892386c698d2",
    "price": 350,
    "created_at": "2018-10-20T20:28:59.385064691+05:30"
  },
  {
    "user_id": "3de7d031-b067-4349-95df-892386c698d2",
    "price": 450,
    "created_at": "2018-10-20T20:29:14.120480468+05:30"
  }
]
```
#### List bids of an user
```
curl http://localhost:8000/v1/users/1DE7D031-B067-4349-95DF-111111111111/bids
```
sample output:
```json
[
  {
    "user_id": "1de7d031-b067-4349-95df-111111111111",
    "price": 100,
    "created_at": "2018-10-20T20:52:35.839525187+05:30"
  },
  {
    "user_id": "1de7d031-b067-4349-95df-111111111111",
    "price": 250,
    "created_at": "2018-10-20T20:52:35.887881374+05:30"
  }
]
```

* for listing bids for user/item, or listing items `204` is returned if list is empty.
