package server

import (
	"net/http"

	"bitbucket.org/devdinu/auction_service/auction"
	"bitbucket.org/devdinu/auction_service/bids"
	"bitbucket.org/devdinu/auction_service/config"
	"bitbucket.org/devdinu/auction_service/logger"
	"bitbucket.org/devdinu/auction_service/ping"
	"bitbucket.org/devdinu/auction_service/store"
	"github.com/gorilla/mux"
)

type Server struct{}

func (s *Server) Start() error {
	logger.Setup(config.OutputWriter(), "")

	memStore := store.NewMemoryStore()
	var auctionCreator http.Handler
	auctionService := auction.NewService(memStore)
	auctionCreator = auction.Creator{Service: auctionService}
	bidService := bids.NewService(memStore)

	router := mux.NewRouter()
	router.HandleFunc("/ping", ping.PingHandler)
	router.Use(headersMiddleware)

	router.Handle("/v1/item", auctionCreator).Methods("POST")
	router.Handle("/v1/items", auction.Lister(auctionService)).Methods("GET")
	router.Handle("/v1/items/{item_id}/bids/winner", auction.Winner(auctionService)).Methods("GET")

	router.Handle("/v1/items/{item_id}/bid", bids.BidCreator(bidService)).Methods("PUT")
	router.Handle("/v1/items/{item_id}/bids", bids.ItemLister(bidService)).Methods("GET")

	router.Handle("/v1/users/{user_id}/bids", bids.UserLister(bidService)).Methods("GET")

	port := config.AppPort()
	logger.Infof("Listening on server port %s\n", port)
	return http.ListenAndServe(port, router)
}

func New() *Server {
	return &Server{}
}

func headersMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}
