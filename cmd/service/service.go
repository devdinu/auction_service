package main

import (
	"fmt"

	"bitbucket.org/devdinu/auction_service/config"

	"bitbucket.org/devdinu/auction_service/server"
)

func main() {
	err := config.Load()
	if err != nil {
		panic(fmt.Errorf("[ConfigLoad] error loading parser config: %v", err))
	}
	s := server.New()
	if err := s.Start(); err != nil {
		panic(err)
	}
}
