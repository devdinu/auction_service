FROM golang:latest

RUN mkdir -p ${GOPATH}/src/bitbucket.org/devdinu
COPY . ${GOPATH}/src/bitbucket.org/devdinu/auction_service

WORKDIR ${GOPATH}/src/bitbucket.org/devdinu/auction_service

RUN env
RUN make install-deps install

WORKDIR ${GOPATH}/bin

EXPOSE 8080
EXPOSE 8000

CMD ["./service"]
