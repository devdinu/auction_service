user1='1DE7D031-B067-4349-95DF-111111111111'
user2='2DE7D031-B067-4349-95DF-222222222222'
user3='3DE7D031-B067-4349-95DF-333333333333'

mobile_id="31d4d6e3-4af7-4ea4-80f6-e4ac139902d8"
art_id="31d4d6e3-4af7-4ea4-80f6-e4ac139902d8"

SERVICE_PORT=${SERVICE_PORT:-8080}
echo "url: localhost:${SERVICE_PORT}"

echo "\ncreate auction mobile:"
curl http://localhost:$SERVICE_PORT/v1/item -d '{"title":"mobile", "description":"mobile version x.x","id":"31d4d6e3-4af7-4ea4-80f6-e4ac139902d8"}' -s
echo "\ncreate auction artwork:"
curl http://localhost:$SERVICE_PORT/v1/item -d '{"title":"picasso artwork", "description":"famouos artwork","id":"31d4d6e3-4af7-4ea4-80f6-e4ac139902d8"}' -s
echo "mobile: $mobile_id, art: $art_id"

curl -X PUT "http://localhost:$SERVICE_PORT/v1/items/$mobile_id/bid" -d "{\"user_id\":\"$user1\",\"price\":100.00}" -s > /dev/null
curl -X PUT "http://localhost:$SERVICE_PORT/v1/items/$mobile_id/bid" -d "{\"user_id\":\"$user2\",\"price\":200.00}" -s > /dev/null
curl -X PUT "http://localhost:$SERVICE_PORT/v1/items/$mobile_id/bid" -d "{\"user_id\":\"$user1\",\"price\":150.00}" -s > /dev/null # invalid request
curl -X PUT "http://localhost:$SERVICE_PORT/v1/items/$mobile_id/bid" -d "{\"user_id\":\"$user1\",\"price\":250.00}" -s > /dev/null

echo "\nuser1 $user1 bids:"
curl -s http://localhost:$SERVICE_PORT/v1/users/${user1}/bids | python -m json.tool
echo "\nuser2 $user2 bids:"
curl -s http://localhost:$SERVICE_PORT/v1/users/${user2}/bids | python -m json.tool

echo "\nitem bids: $mobile_id"
curl -s http://localhost:$SERVICE_PORT/v1/items/${mobile_id}/bids | python -m json.tool

echo "\n item sold to bid: "
curl http://localhost:$SERVICE_PORT/v1/items/31d4d6e3-4af7-4ea4-80f6-e4ac139902d8/bids/winner | python -m json.tool
