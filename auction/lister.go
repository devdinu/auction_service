package auction

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/devdinu/auction_service/logger"
)

func Lister(svc Service) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		auctions, err := svc.ListAuctions(r.Context())
		if err != nil {
			logger.Errorf("[AuctionLister] Can't list auctions, err: ", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if len(auctions) == 0 {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		data, err := json.Marshal(auctions)
		if err != nil {
			logger.Errorf("[AuctionLister] Can't write response for user")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Write(data)
	})
}
