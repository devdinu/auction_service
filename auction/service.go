package auction

import (
	"context"

	"bitbucket.org/devdinu/auction_service/store"
	uuid "github.com/satori/go.uuid"
)

type auctionStore interface {
	SaveAuction(context.Context, store.Auction) error
	ListAuctions(context.Context) ([]store.Auction, error)
	Winner(context.Context, uuid.UUID) (store.Bid, error)
}

type Service struct {
	auctionStore auctionStore
}

func (s Service) SaveAuction(ctx context.Context, acn auctionItem) (uuid.UUID, error) {
	acnID, err := uuid.FromString(acn.ID)
	if err != nil {
		acnID = uuid.NewV4()
	}
	sacn := store.Auction{ID: acnID, Title: acn.Title, Description: acn.Description}
	return acnID, s.auctionStore.SaveAuction(ctx, sacn)
}

func (s Service) Winner(ctx context.Context, itemID uuid.UUID) (store.Bid, error) {
	//TOOD: close auction
	return s.auctionStore.Winner(ctx, itemID)
}
func (s Service) ListAuctions(ctx context.Context) ([]store.Auction, error) {
	return s.auctionStore.ListAuctions(ctx)
}

func NewService(store auctionStore) Service {
	return Service{auctionStore: store}
}
