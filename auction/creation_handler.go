package auction

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/devdinu/auction_service/logger"
)

type auctionItem struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	ID          string `json:"id"`
}

type response struct {
	ID string `json:"id"`
}

type Creator struct {
	Service
}

func (c Creator) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var item auctionItem
	err := json.NewDecoder(r.Body).Decode(&item)
	if err != nil {
		logger.Errorf("[AuctionCreator] failed to decode request")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	id, err := c.Service.SaveAuction(r.Context(), item)
	if err != nil {
		logger.Errorf("[AuctionCreator] failed to save auction: %s", id.String())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	data, err := json.Marshal(response{ID: id.String()})
	if err != nil {
		logger.Errorf("[AuctionCreator] failed to write response: %s", id.String())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
	w.Write(data)
}
