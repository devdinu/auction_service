package auction

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/devdinu/auction_service/logger"
	"github.com/gorilla/mux"
	"github.com/satori/go.uuid"
)

func Winner(svc Service) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		itemID, err := uuid.FromString(mux.Vars(r)["item_id"])
		if itemID.String() == "" {
			logger.Errorf("[AuctionWinner] Invalid request for winner bid, item id: %s", itemID)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		highestBid, err := svc.Winner(r.Context(), itemID)
		if err != nil {
			logger.Errorf("[AuctionWinner] Can't get highest bid for item: %s, err: ", itemID, err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		data, err := json.Marshal(highestBid)
		if err != nil {
			logger.Errorf("[AuctionWinner]] Can't write response for user")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Write(data)
	})
}
