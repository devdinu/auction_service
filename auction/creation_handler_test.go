package auction

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"bitbucket.org/devdinu/auction_service/store"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestShouldReturnBadRequest(t *testing.T) {
	body := `{"invalid_request"}`
	r, _ := http.NewRequest("GET", "/some_url", strings.NewReader(body))
	w := httptest.NewRecorder()

	creator := Creator{NewService(new(storeMock))}

	creator.ServeHTTP(w, r)

	assert.Equal(t, 400, w.Code)
}

func TestShouldSaveAuctionSuccessfully(t *testing.T) {
	itemID := uuid.NewV4()
	body := fmt.Sprintf(`{"title": "item1", "description": "item_description", "id": "%s"}`, itemID.String())
	r, _ := http.NewRequest("GET", "/some_url", strings.NewReader(body))
	r = r.WithContext(context.Background())
	w := httptest.NewRecorder()
	auctionStore := new(storeMock)
	creator := Creator{NewService(auctionStore)}
	auctionStore.On("SaveAuction", r.Context(), store.Auction{Title: "item1", Description: "item_description", ID: itemID}).Return(nil)

	creator.ServeHTTP(w, r)

	assert.Equal(t, 201, w.Code)
	var resp response
	err := json.NewDecoder(w.Body).Decode(&resp)
	require.NoError(t, err)

	assert.Equal(t, itemID.String(), resp.ID)
	auctionStore.AssertExpectations(t)
}

type storeMock struct{ mock.Mock }

func (m *storeMock) SaveAuction(ctx context.Context, a store.Auction) error {
	args := m.Called(ctx, a)
	return args.Error(0)
}

func (m *storeMock) ListAuctions(ctx context.Context) ([]store.Auction, error) {
	args := m.Called(ctx)
	return args.Get(0).([]store.Auction), args.Error(1)
}

func (m *storeMock) Winner(ctx context.Context, itemID uuid.UUID) (store.Bid, error) {
	args := m.Called(ctx, itemID)
	return args.Get(0).(store.Bid), args.Error(1)
}
